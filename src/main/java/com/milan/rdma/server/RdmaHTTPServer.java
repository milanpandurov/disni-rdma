package com.milan.rdma.server;

import com.ibm.disni.rdma.RdmaActiveEndpointGroup;
import com.ibm.disni.rdma.RdmaEndpointFactory;
import com.ibm.disni.rdma.RdmaServerEndpoint;
import com.ibm.disni.rdma.verbs.*;
import com.ibm.disni.util.GetOpt;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.LinkedList;

/**
 * Created by milan on 5/6/17.
 */
public class RdmaHTTPServer implements RdmaEndpointFactory<ServerEndpoint>{
    private String ipAddress;
    private File fileDirectory;
    public boolean shouldRun=true;

    RdmaActiveEndpointGroup<ServerEndpoint> endpointGroup;

    public ServerEndpoint createEndpoint(RdmaCmId cmd, boolean serverSide) throws IOException {
        return new ServerEndpoint(endpointGroup, cmd, serverSide);
    }

    public void run() throws Exception {
        //create a EndpointGroup. The RdmaActiveEndpointGroup contains CQ processing and delivers CQ event to the endpoint.dispatchCqEvent() method.
        endpointGroup = new RdmaActiveEndpointGroup<ServerEndpoint>(1000, false, 128, 4, 128);
        endpointGroup.init(this);
        //create a server endpoint
        RdmaServerEndpoint<ServerEndpoint> serverEndpoint = endpointGroup.createServerEndpoint();

        URI uri = URI.create("rdma://" + ipAddress + ":" + 1919);
        serverEndpoint.bind(uri);
        System.out.println("RDMA server bind to address: " + uri.toString());

        while(shouldRun) {
            ServerEndpoint clientEndpoint = serverEndpoint.accept();
            System.out.println("New request received");
            RequestHandler rh = new RequestHandler(clientEndpoint);
            Thread t = new Thread(rh);
            t.start();
        }

        serverEndpoint.close();
        System.out.println("server endpoint closed");
        endpointGroup.close();
        System.out.println("group closed");
    }

    public void launch(String[] args) throws Exception {
        String[] _args = args;
        if (args.length < 1) {
            System.err.println("You must specify IP address");
            System.exit(0);
        }

        GetOpt go = new GetOpt(_args, "a:d:");
        go.optErr = true;

        int ch = -1;

        while ((ch = go.getopt()) != GetOpt.optEOF) {
            if ((char) ch == 'a') {
                ipAddress = go.optArgGet();
            }
            if ((char) ch == 'd') {
                String dir = go.optArgGet();

                fileDirectory=new File(dir);
                if(!fileDirectory.exists() || !fileDirectory.isDirectory()){
                    throw new Exception("Provided path is not a directory!");
                }
                System.out.println("Static files directory: "+fileDirectory.getAbsolutePath());
            }
        }

        this.run();
    }

    class RequestHandler implements Runnable{
        private ServerEndpoint endpoint;
        public boolean shouldRun=true;

        public RequestHandler(ServerEndpoint endpoint){
            super();
            this.endpoint=endpoint;
        }

        public void handleRequests() throws InterruptedException, IOException {
            ByteBuffer dataBuff=endpoint.getDataBuf();
            IbvMr dataMr= endpoint.getDataMr();

            endpoint.getWcEvents().take();
            System.out.println("Got handshake!");

            ByteBuffer recvBuf = endpoint.getRecvBuf();
            recvBuf.clear();
            String handshakeMessage=recvBuf.asCharBuffer().toString();
            System.out.println("Got handshake message: "+handshakeMessage);
            endpoint.executeReceive();

            ByteBuffer sendBuf = endpoint.getSendBuf();
            sendBuf.clear();

            sendBuf.putLong(dataMr.getAddr());
            sendBuf.putInt(dataMr.getLkey());
            endpoint.executeSend();

            System.out.println("Sending buffer info");

            endpoint.getWcEvents().take();

            System.out.println("Handshake finished!");

            while(shouldRun){
                endpoint.getWcEvents().take();
                System.out.println("Got request!");

                recvBuf.clear();
                String requestFile=recvBuf.asCharBuffer().toString();

                System.out.println("Received file request: "+requestFile);
                requestFile=requestFile.trim();

                File requestFilePath=new File(fileDirectory, requestFile);
                System.out.println("Looking for file: "+requestFilePath);

                if(requestFilePath.exists() && requestFilePath.isFile()){
                    InputStream targetStream = new FileInputStream(requestFilePath);

                    byte[] fileContent=IOUtils.toByteArray(targetStream);

                    int length=fileContent.length;
                    int currentPosition=0;

                    sendBuf.clear();
                    sendBuf.putInt(200);
                    sendBuf.putInt(length);
                    System.out.println("File exists, sending 200");

                    int i=1;
                    while (length>0){

                        int sendSize=Math.min(length, endpoint.getBufferSize());

                        System.out.println("Sending chunk of size "+sendSize+" I: "+i);
                        i++;

                        dataBuff.clear();
                        dataBuff.put(fileContent, currentPosition, sendSize);

                        length-=sendSize;
                        currentPosition+=sendSize;

                        endpoint.executeReceive();
                        endpoint.executeSend();

                        System.out.println("Read notification sent!");
                        endpoint.getWcEvents().take();
                        System.out.println("Client read it");
                        endpoint.getWcEvents().take();
                        System.out.println("He acked it");
                    }

                    endpoint.executeReceive();
                    endpoint.executeSend();
                    endpoint.getWcEvents().take();

                }else{
                    System.out.println("File not found, sending 404!");
                    sendBuf.clear();
                    sendBuf.putInt(404);
                    sendBuf.putInt(0);

                    endpoint.executeReceive();
                    endpoint.executeSend();
                    endpoint.getWcEvents().take();
                }

                System.out.println("Message sent! Waiting for new request");
            }
        }

        public void run() {
            try {
                this.handleRequests();
            }catch (Exception e){
                System.err.println("Error occure while processing requests!");
                e.printStackTrace();
            }

            try {
                endpoint.close();
            }catch (Exception e){
                System.err.println("Error occure while closing client endpoint!");
                e.printStackTrace();
            }
            System.out.println("Finished processing request");
        }
    }

    public static void main(String[] args){
        System.out.println("Starting RDMA HTTP Server");
        RdmaHTTPServer rdmaHttpServer = new RdmaHTTPServer();
        try {
            rdmaHttpServer.launch(args);
            System.out.println("Bye");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
