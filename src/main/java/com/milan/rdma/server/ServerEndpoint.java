package com.milan.rdma.server;

import com.ibm.disni.rdma.RdmaActiveEndpoint;
import com.ibm.disni.rdma.RdmaActiveEndpointGroup;
import com.ibm.disni.rdma.verbs.*;
import com.milan.rdma.utils.CustomEndpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by milan on 5/6/17.
 */
public class ServerEndpoint extends CustomEndpoint {
    public ServerEndpoint(RdmaActiveEndpointGroup<ServerEndpoint> endpointGroup, RdmaCmId idPriv, boolean serverSide) throws IOException {
        super(endpointGroup, idPriv, serverSide, 2500);
        this.buffercount = 3;
        this.buffersize = 200;
        buffers = new ByteBuffer[buffercount];
        this.mrlist = new IbvMr[buffercount];

        for (int i = 0; i < buffercount; i++){
            buffers[i] = ByteBuffer.allocateDirect(buffersize);
        }

        this.wrList_send = new LinkedList<IbvSendWR>();
        this.sgeSend = new IbvSge();
        this.sgeList = new LinkedList<IbvSge>();
        this.sendWR = new IbvSendWR();

        this.wrList_recv = new LinkedList<IbvRecvWR>();
        this.sgeRecv = new IbvSge();
        this.sgeListRecv = new LinkedList<IbvSge>();
        this.recvWR = new IbvRecvWR();

        this.wcEvents = new ArrayBlockingQueue<IbvWC>(20);
    }

    public void init() throws IOException{
        super.init();

        this.prepareReceiveBuffer();
        this.executeReceive();
        this.prepareSendBuffer();

        this.executeReceive();
    }

}
