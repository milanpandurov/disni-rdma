package com.milan.rdma.utils;

import com.ibm.disni.rdma.RdmaActiveEndpoint;
import com.ibm.disni.rdma.RdmaActiveEndpointGroup;
import com.ibm.disni.rdma.verbs.*;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by milan on 5/14/17.
 */
public abstract class CustomEndpoint extends RdmaActiveEndpoint {
    protected ByteBuffer buffers[];
    protected IbvMr mrlist[];
    protected int buffercount = 3;
    protected int buffersize = 200;

    protected ByteBuffer dataBuf;
    protected IbvMr dataMr;
    protected ByteBuffer sendBuf;
    protected IbvMr sendMr;
    protected ByteBuffer recvBuf;
    protected IbvMr recvMr;

    protected LinkedList<IbvSendWR> wrList_data;
    protected IbvSge sgeData;
    protected LinkedList<IbvSge> sgeListData;
    protected IbvSendWR dataWR;

    protected LinkedList<IbvSendWR> wrList_send;
    protected IbvSge sgeSend;
    protected LinkedList<IbvSge> sgeList;
    protected IbvSendWR sendWR;

    protected LinkedList<IbvRecvWR> wrList_recv;
    protected IbvSge sgeRecv;
    protected LinkedList<IbvSge> sgeListRecv;
    protected IbvRecvWR recvWR;

    protected ArrayBlockingQueue<IbvWC> wcEvents;

    protected int lastId=1000;

    public CustomEndpoint(RdmaActiveEndpointGroup<? extends RdmaActiveEndpoint> group, RdmaCmId idPriv, boolean serverSide, int lastId) throws IOException {
        super(group, idPriv, serverSide);
        this.lastId=lastId;

        this.buffercount = 3;
        this.buffersize = 200;
        buffers = new ByteBuffer[buffercount];
        this.mrlist = new IbvMr[buffercount];

        for (int i = 0; i < buffercount; i++){
            buffers[i] = ByteBuffer.allocateDirect(buffersize);
        }

        this.wrList_send = new LinkedList<IbvSendWR>();
        this.sgeSend = new IbvSge();
        this.sgeList = new LinkedList<IbvSge>();
        this.sendWR = new IbvSendWR();

        this.wrList_recv = new LinkedList<IbvRecvWR>();
        this.sgeRecv = new IbvSge();
        this.sgeListRecv = new LinkedList<IbvSge>();
        this.recvWR = new IbvRecvWR();

        this.wrList_data = new LinkedList<IbvSendWR>();
        this.sgeData = new IbvSge();
        this.sgeListData = new LinkedList<IbvSge>();
        this.dataWR = new IbvSendWR();

        this.wcEvents = new ArrayBlockingQueue<IbvWC>(20);
    }

    @Override
    protected synchronized void init() throws IOException {
        super.init();

        for (int i = 0; i < buffercount; i++){
            mrlist[i] = registerMemory(buffers[i]).execute().free().getMr();
        }

        this.dataBuf = buffers[0];
        this.dataMr = mrlist[0];
        this.sendBuf = buffers[1];
        this.sendMr = mrlist[1];
        this.recvBuf = buffers[2];
        this.recvMr = mrlist[2];
    }

    public void cleanup() throws IOException {
        for (int i = 0; i < buffercount; i++){
            deregisterMemory(mrlist[i]);
        }
    }

    public int prepareSendBuffer(){
        int lid=lastId;
        System.out.println("Preparing send buffer with id "+lastId);
        sgeSend.setAddr(sendMr.getAddr());
        sgeSend.setLength(sendMr.getLength());
        sgeSend.setLkey(sendMr.getLkey());
        sgeList.add(sgeSend);
        sendWR.setWr_id(lastId+1);
        sendWR.setSg_list(sgeList);
        sendWR.setOpcode(IbvSendWR.IBV_WR_SEND);
        sendWR.setSend_flags(IbvSendWR.IBV_SEND_SIGNALED);
        wrList_send.clear();
        wrList_send.add(sendWR);
        return lid;
    }

    public int prepareRead(long remoteAddr, int lkey){
        int lid=lastId;
        System.out.println("Preparing read buffer with id "+lastId);

        sgeData.setAddr(dataMr.getAddr());
        sgeData.setLength(dataMr.getLength());
        sgeData.setLkey(dataMr.getLkey());
        sgeListData.add(sgeData);
        dataWR.getRdma().setRemote_addr(remoteAddr);
        dataWR.getRdma().setRkey(lkey);
        dataWR.setWr_id(lastId+1);
        dataWR.setSg_list(sgeListData);
        dataWR.setOpcode(IbvSendWR.IBV_WR_RDMA_READ);
        dataWR.setSend_flags(IbvSendWR.IBV_SEND_SIGNALED);
        wrList_data.add(dataWR);
        return lid;
    }

    public void executeSend() throws IOException{
        System.out.println("SimpleServer::initiated send with "+wrList_send.size()+" elements");
        this.postSend(wrList_send).execute().free();
        System.out.println("SimpleServer::wrList send finished");
    }

    public int prepareReceiveBuffer(){
        int lid=lastId;
        System.out.println("Preparing receive buffer with id "+lastId);
        sgeRecv.setAddr(recvMr.getAddr());
        sgeRecv.setLength(recvMr.getLength());
        int lkey = recvMr.getLkey();
        sgeRecv.setLkey(lkey);
        sgeListRecv.add(sgeRecv);
        recvWR.setSg_list(sgeListRecv);
        recvWR.setWr_id(lastId);
        wrList_recv.clear();
        wrList_recv.add(recvWR);

        return lid;
    }

    @Override
    public void close() throws IOException, InterruptedException {
        super.close();

        for (int i = 0; i < buffercount; i++){
            deregisterMemory(mrlist[i]);
        }
    }

    public void executeReceive() throws IOException{
        System.out.println("SimpleServer::initiated recv with "+wrList_recv.size()+" elements");
        this.postRecv(wrList_recv).execute().free();
        System.out.println("SimpleServer::wrList recv finished");
    }

    public void dispatchCqEvent(IbvWC wc) throws IOException {
        System.out.println(">> Event received! "+wc.getWr_id());
        wcEvents.add(wc);
    }

    public ArrayBlockingQueue<IbvWC> getWcEvents() {
        return wcEvents;
    }

    public LinkedList<IbvSendWR> getWrList_send() {
        return wrList_send;
    }

    public LinkedList<IbvSendWR> getWrList_data() {
        return wrList_data;
    }

    public LinkedList<IbvRecvWR> getWrList_recv() {
        return wrList_recv;
    }

    public ByteBuffer getDataBuf() {
        return dataBuf;
    }

    public ByteBuffer getSendBuf() {
        return sendBuf;
    }

    public ByteBuffer getRecvBuf() {
        return recvBuf;
    }

    public IbvSendWR getSendWR() {
        return sendWR;
    }

    public IbvRecvWR getRecvWR() {
        return recvWR;
    }

    public IbvMr getDataMr() {
        return dataMr;
    }

    public IbvMr getSendMr() {
        return sendMr;
    }

    public IbvMr getRecvMr() {
        return recvMr;
    }

    public int getBufferSize(){
        return buffersize;
    }
}
