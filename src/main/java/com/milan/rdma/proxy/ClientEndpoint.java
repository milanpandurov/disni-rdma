package com.milan.rdma.proxy;

import com.ibm.disni.rdma.RdmaActiveEndpoint;
import com.ibm.disni.rdma.RdmaActiveEndpointGroup;
import com.ibm.disni.rdma.verbs.*;
import com.milan.rdma.utils.CustomEndpoint;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by milan on 5/7/17.
 */
public class ClientEndpoint extends CustomEndpoint {

    public ClientEndpoint(RdmaActiveEndpointGroup<ClientEndpoint> endpointGroup, RdmaCmId idPriv, boolean serverSide) throws IOException {
        super(endpointGroup, idPriv, serverSide, 2000);
    }

    @Override
    protected synchronized void init() throws IOException {
        super.init();

        this.prepareSendBuffer();
        this.prepareReceiveBuffer();
    }
}
