package com.milan.rdma.proxy;

import com.ibm.disni.rdma.RdmaActiveEndpointGroup;
import com.ibm.disni.rdma.RdmaEndpointFactory;
import com.ibm.disni.rdma.verbs.IbvSendWR;
import com.ibm.disni.rdma.verbs.IbvWC;
import com.ibm.disni.rdma.verbs.RdmaCmId;
import com.ibm.disni.rdma.verbs.SVCPostSend;
import com.ibm.disni.util.GetOpt;
import com.milan.rdma.server.RdmaHTTPServer;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.nio.ByteBuffer;

/**
 * Created by milan on 5/6/17.
 */
public class ProxyServer implements RdmaEndpointFactory<ClientEndpoint>, HttpHandler, Runnable {
    private String ipAddress;
    private ClientEndpoint endpoint;
    private boolean connected;

    public void run() {
        while(!connected) {
            System.out.println("Starting to initialize connection");
            try {
                endpoint = endpointGroup.createEndpoint();
                System.out.println("Endpoint created");
                endpoint.connect(URI.create("rdma://" + ipAddress + ":" + 1919));
                System.out.println("SimpleClient::client channel set up ");

                endpoint.executeReceive();
                ByteBuffer sendBuf = endpoint.getSendBuf();
                sendBuf.clear();
                System.out.println("Initializing connection!");
                sendBuf.asCharBuffer().put("Hello");
                sendBuf.clear();
                endpoint.executeSend();

                IbvWC wc = endpoint.getWcEvents().take();
                System.out.println("Hello sent");
                wc = endpoint.getWcEvents().take();

                System.out.println("Read buffer info received, setting data up!");

                ByteBuffer recvBuf = endpoint.getRecvBuf();
                recvBuf.clear();
                long addr = recvBuf.getLong();
                int lkey = recvBuf.getInt();

                endpoint.prepareRead(addr, lkey);

                System.out.println("Finished handshake!");

                connected = true;
            } catch (Exception e) {
                System.err.println("Error establishing connection");
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException ee){
                    ee.printStackTrace();
                }
            }
        }
        System.out.println("Finished initialization!");
    }

    public class RequestException extends RuntimeException{
        private int httpCode;
        private String message;

        public RequestException(int code, String message){
            super();
            this.httpCode=code;
            this.message=message;
        }

        public int getHttpCode(){
            return this.httpCode;
        }

        public String getMessage(){
            return this.message;
        }
    }

    RdmaActiveEndpointGroup<ClientEndpoint> endpointGroup;

    public ProxyServer(String ipAddress){
        super();
        this.ipAddress=ipAddress;
        this.connected=false;
    }

    public ClientEndpoint createEndpoint(RdmaCmId idPriv, boolean serverSide) throws IOException{
        return new ClientEndpoint(endpointGroup, idPriv, serverSide);
    }

    public void handle(HttpExchange t) throws IOException {
        OutputStream os = t.getResponseBody();

        if(!connected){
            String response = "Gateway not connected";
            t.sendResponseHeaders(504, response.length());
            os.write(response.getBytes());

        }else if(t.getRequestURI().getHost().equals("www.rdmawebpage.com")){

            String requestedFile=t.getRequestURI().getPath();
            if (requestedFile.equals("/")){
                System.out.println("Request file not specified, defaulting to index.html");
                requestedFile="/index.html";
            }

            try {
                this.sendSimpleRequest(requestedFile, t);
            }catch (RequestException e){
                String response = e.getMessage();
                t.sendResponseHeaders(e.httpCode, response.length());
                os.write(response.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
                String response = "Gateway timed out";
                t.sendResponseHeaders(504, response.length());
                os.write(response.getBytes());
                connected=false;
                this.cleanup();
                startInitialization();
            }
        }else{
            String response = "Not found! This proxy only resolves www.rdmawebpage.com requests";
            t.sendResponseHeaders(404, response.length());
            os.write(response.getBytes());
        }

        os.close();
    }

    private void startInitialization(){
        Thread t = new Thread(this);
        t.start();
    }

    public void init()throws Exception{
        endpointGroup = new RdmaActiveEndpointGroup<ClientEndpoint>(1000, false, 128, 4, 128);
        endpointGroup.init(this);
        startInitialization();

    }

    private void cleanup(){
        try {
            endpoint.cleanup();
            endpoint.close();
        }catch (Exception e){
            System.err.println("Error closing connection");
            e.printStackTrace();
        }
    }

    public void sendSimpleRequest(String filename, HttpExchange t) throws IOException, InterruptedException {
        endpoint.executeReceive();

        ByteBuffer sendBuf = endpoint.getSendBuf();
        System.out.println("Requesting: "+filename+" length "+filename.length());
        sendBuf.clear();
        sendBuf.put(new byte[sendBuf.capacity()]);
        sendBuf.clear();
        sendBuf.asCharBuffer().put(filename);
        System.out.println(sendBuf.asCharBuffer().toString());
        endpoint.executeSend();

        IbvWC wc = endpoint.getWcEvents().take();

        wc = endpoint.getWcEvents().take();

        System.out.println("Message received");
        ByteBuffer recvBuf = endpoint.getRecvBuf();
        recvBuf.clear();
        int responseStatus=recvBuf.getInt();
        int length=recvBuf.getInt();

        if(responseStatus==200){
            ByteBuffer dataBuf=endpoint.getDataBuf();
            t.sendResponseHeaders(responseStatus, length);
            OutputStream os=t.getResponseBody();
            System.out.println("Got 200! Starting to receive file!");

            int i=1;
            while (length>0){

                int readSize=Math.min(length, dataBuf.capacity());

                System.out.println("Reading chunk of size "+readSize+" I: "+i);
                i++;

                SVCPostSend postData = endpoint.postSend(endpoint.getWrList_data());
                postData.getWrMod(0).getSgeMod(0).setLength(readSize);
                postData.execute();

                endpoint.getWcEvents().take();
                System.out.println("Have data in my buffer");

                dataBuf.clear();
                byte[] data = new byte[readSize];
                dataBuf.get(data);
                System.out.println("Sending that part");
                os.write(data);

                length-=readSize;

                endpoint.executeReceive();
                sendBuf.clear();
                System.out.println("Notifying sender that im ready");
                endpoint.executeSend();

                endpoint.getWcEvents().take();
                System.out.println("Message sent!");
                endpoint.getWcEvents().take();
                System.out.println("Done!");
            }

        }else{
            RequestException re=null;
            if(responseStatus==404){
                re=new RequestException(responseStatus, "File not found!");
            }else if(responseStatus==500){
                re=new RequestException(responseStatus, "RDMA server internal error");
            }else{
                re=new RequestException(responseStatus, "Unknown error");
            }

            throw re;
        }
    }

}
