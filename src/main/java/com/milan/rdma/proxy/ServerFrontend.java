package com.milan.rdma.proxy;

import com.ibm.disni.util.GetOpt;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;

/**
 * Created by milan on 5/6/17.
 */
public class ServerFrontend {
    private int serverPort;
    private HttpHandler requestHandler;

    public ServerFrontend(int port, HttpHandler requestHandler){
        this.serverPort=port;
        this.requestHandler=requestHandler;
    }

    public void launch() throws IOException{
        HttpServer server = HttpServer.create(new InetSocketAddress(serverPort), 0);
        server.createContext("/", requestHandler);
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    public static void main(String[] args){
        String[] _args = args;
        if (args.length < 1) {
            System.err.println("You must specify IP address");
            System.exit(1);
        }

        GetOpt go = new GetOpt(_args, "a:p:");
        go.optErr = true;
        int ch = -1;

        String ipAddress=null;
        int port=8080;

        while ((ch = go.getopt()) != GetOpt.optEOF) {
            if ((char) ch == 'a') {
                ipAddress = go.optArgGet();
            }
            if ((char) ch == 'p') {
                port = Integer.parseInt(go.optArgGet());
            }
        }

        if(ipAddress==null){
            System.err.println("Error! You must specify IP address!");
            System.exit(-1);
        }

        try {
            ProxyServer ps=new ProxyServer(ipAddress);
            ps.init();
            ServerFrontend sf=new ServerFrontend(port, ps);
            System.out.println("Starting frontend server!");
            sf.launch();

        }catch (Exception e){
            System.err.println("Error running proxy!");
            e.printStackTrace();
            System.exit(-1);
        }

    }
}
